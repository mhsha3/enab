<?php

namespace App\Http\Controllers;

use App\BusinessSetting;
use App\Http\Controllers\TapPayment;
use Illuminate\Http\Request;
use App\Order;
use App\Seller;
use App\Http\Controllers\Controller;
use App\CustomerPackage;
use App\SellerPackage;
use App\Http\Controllers\CustomerPackageController;
use Auth;
use Session;

class TapPaymentController extends Controller
{

    public function charge(Request $request)
    {

        if(Session::get('payment_type') == 'cart_payment'){
            $order = Order::findOrFail(Session::get('order_id'));
            $customer_name = $request->session()->get('shipping_info')['name'];
            $customer_email = $request->session()->get('shipping_info')['email'];
            $amount = $order->grand_total;
            $description = 'Cart Payment';
        }
        elseif(Session::get('payment_type') == 'wallet_payment'){
            $user = Auth::user(); 
            $customer_name = $user->name;
            $customer_email = $user->email;
            $amount = Session::get('payment_data')['amount'];
            $description = 'Wallet Payment';
        }
        elseif(Session::get('payment_type') == 'customer_package_payment'){
            $customer_package = CustomerPackage::findOrFail(Session::get('payment_data')['customer_package_id']);
            $user = Auth::user(); 
            $customer_name = $user->name;
            $customer_email = $user->email;
            $amount = $customer_package->amount;
            $description = 'Customer Package Payment';
        }
        elseif(Session::get('payment_type') == 'seller_package_payment'){
            $seller_package = SellerPackage::findOrFail(Session::get('payment_data')['seller_package_id']);
            $user = Auth::user(); 
            $customer_name = $user->name;
            $customer_email = $user->email;
            $amount = $seller_package->amount;
            $description = 'Seller Package Payment';
        }

        $unique_ref_md5 = md5(uniqid (rand (),true));
        $request->session()->put('tap_reference_transaction', $unique_ref_md5);

        $charge_data = [
            'amount' => $amount,
            'currency' => env('TAP_CURRENCY'),
            'threeDSecure' => (env('TAP_3D_SECURE') == '1' ? 'true' : 'false'),
            'description' => $description,
            'customer' => [
                'first_name' => $customer_name,
                'email' => $customer_email,
            ],
            'source' => [
                'id' => env('TAP_SOURCE_ID')
            ],
            'merchant' => [
                'id' => env('TAP_MERCHANT_ID')
            ],
            'reference' => [
                'transaction' => $unique_ref_md5
            ],
            'redirect' => [
                'url' => route('tap.redirect')
            ]
        ];

        $tapPayment = new TapPayment(['secret_api_Key' => env('TAP_SECRET_API_KEY')]);
        return $tapPayment->charge($charge_data);
    }

    public function redirect(Request $request)
    {
        try {
            if(Session::has('tap_reference_transaction') && Session::has('payment_type')) {
                    
                $tap_id = $request->get('tap_id');
                $tapPayment = new TapPayment(['secret_api_Key' => env('TAP_SECRET_API_KEY')]);
                
                $charge_data = $tapPayment->getCharge($tap_id);
                $local_ref_md5 = Session::get('tap_reference_transaction');
                Session::forget('tap_reference_transaction');

                if($charge_data && $charge_data->reference->transaction == $local_ref_md5) {

                    $payment_data = json_encode ((array) $charge_data);

                    if(Session::get('payment_type') == 'cart_payment'){
                        $checkoutController = new CheckoutController;
                        return $checkoutController->checkout_done(Session::get('order_id'), $payment_data);
                    }
                    elseif (Session::get('payment_type') == 'wallet_payment') {
                        $walletController = new WalletController;
                        return $walletController->wallet_payment_done(Session::get('payment_data'), $payment_data);
                    }
                    elseif (Session::get('payment_type') == 'customer_package_payment') {
                        $customer_package_controller = new CustomerPackageController;
                        return $customer_package_controller->purchase_payment_done(Session::get('payment_data'), $payment_data);
                    }
                    elseif (Session::get('payment_type') == 'seller_package_payment') {
                        $seller_package_controller = new SellerPackageController;
                        return $seller_package_controller->purchase_payment_done(Session::get('payment_data'), $payment_data);
                    }
                } else {
                    throw new \Exception("Invalid charge response");
                }
            } else {
                throw new \Exception("Reference missing");
            }
        } catch (\Exception $e) {
            Session::forget('order_id');
            Session::forget('payment_data');
            flash(translate('Payment failed'))->error();
    	    return redirect()->route('home');
        }
    }
}
